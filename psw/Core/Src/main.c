/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * @file           : main.c
 * @brief          : Main program body
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
 * All rights reserved.</center></h2>
 *
 * This software component is licensed by ST under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 ******************************************************************************
 */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "dma.h"
#include "i2c.h"
#include "rtc.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "SSD1306_OLED.h"
#include "GFX_BW.h"
#include "fonts/fonts.h"
#include "bmp280.h"
#include "stdio.h"
#include <stdbool.h>
#include <string.h>

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */
/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define C1_PORT GPIOB
#define C1_PIN GPIO_PIN_10

#define C2_PORT GPIOB
#define C2_PIN GPIO_PIN_3

#define C3_PORT GPIOB
#define C3_PIN GPIO_PIN_5

#define C4_PORT GPIOB
#define C4_PIN GPIO_PIN_4

#define R1_PORT GPIOB
#define R1_PIN GPIO_PIN_13

#define R2_PORT GPIOB
#define R2_PIN GPIO_PIN_14

#define R3_PORT GPIOB
#define R3_PIN GPIO_PIN_15

#define R4_PORT GPIOB
#define R4_PIN GPIO_PIN_1

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
BMP280_t Bmp280;
float Temperature;
float lowestTemperature;
float highestTemperature;
bool firstMeasurement = true;
char Message[32];
uint8_t key;
uint32_t SoftTimerBmp;
uint32_t SoftTimerOled;
RTC_TimeTypeDef RtcTime;
RTC_DateTypeDef RtcDate;
RTC_TimeTypeDef sTime = { 0 };
char mode[12];
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_NVIC_Init(void);
/* USER CODE BEGIN PFP */
static void display_temperatures(void);
void reset_temperatures(void);
static void display_time(void);
float read_temperature(void);
static void display_menu(void);
void read_keypad(void);
void set_time();
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_USART2_UART_Init();
  MX_I2C1_Init();
  MX_RTC_Init();

  /* Initialize interrupts */
  MX_NVIC_Init();
  /* USER CODE BEGIN 2 */
	BMP280_Init(&Bmp280, &hi2c1, 0x76);
	SSD1306_Init(&hi2c1);
	GFX_SetFont(font_8x5);
	SSD1306_Clear(BLACK);
	SSD1306_Display();
	SoftTimerBmp = HAL_GetTick();
	SoftTimerOled = HAL_GetTick();
	HAL_RTC_SetTime(&hrtc, &sTime, RTC_FORMAT_BIN);
	key = 13;

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
	while (1) {
		read_keypad();

		switch (key)
		{
		     case 13:
		    	display_menu();
		    	sTime.Hours++;
		    	break;
		     ;
		     case 14:
		    	while(14 == key){
		    		read_keypad();
		    		while(8 == key){
		    			read_keypad();
		    			read_temperature();
		    			display_temperatures();
		    			}
		    		if(4 == key){
			    		read_temperature();
			    		display_temperatures();
			    		key = 14;
			    		}
		    		display_temperatures();
		    	}
		 		break;
		 	 ;
		     case 15:
				set_time();
				break;
		     ;
		     case 16:
				reset_temperatures();
				break;
		     ;
		     default:
		    	 key = 13;
		    	 break;
		}
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
	}
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI|RCC_OSCILLATORTYPE_LSE;
  RCC_OscInitStruct.LSEState = RCC_LSE_ON;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 16;
  RCC_OscInitStruct.PLL.PLLN = 336;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV4;
  RCC_OscInitStruct.PLL.PLLQ = 4;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInitStruct.PeriphClockSelection = RCC_PERIPHCLK_RTC;
  PeriphClkInitStruct.RTCClockSelection = RCC_RTCCLKSOURCE_LSE;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief NVIC Configuration.
  * @retval None
  */
static void MX_NVIC_Init(void)
{
  /* DMA1_Stream1_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Stream1_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Stream1_IRQn);
  /* I2C1_EV_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(I2C1_EV_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(I2C1_EV_IRQn);
}

/* USER CODE BEGIN 4 */
void set_time(void) {
	SSD1306_Clear(BLACK);
	display_time();
	SSD1306_Display();

	while(15 == key){
		read_keypad();
		if(9 == key){
			sTime.Hours++;
		}
		if(5 == key){
			sTime.Minutes++;
		}
		if(1 == key){
			sTime.Seconds++;
		}
		if(13 == key){
			return;
		}
	}

	if(sTime.Seconds>60){
		sTime.Seconds = 0;
		sTime.Minutes++;
	}
	if(sTime.Minutes>60){
		sTime.Minutes = 0;
		sTime.Hours++;
	}
	if(sTime.Hours>24){
		sTime.Hours = 0;
	}

	HAL_RTC_SetTime(&hrtc, &sTime, RTC_FORMAT_BIN);
	SSD1306_Clear(BLACK);
	display_time();
	key = 15;
	SSD1306_Display();
	HAL_Delay(100);
}

void reset_temperatures(void) {
	lowestTemperature = Temperature;
	highestTemperature = Temperature;
}


float read_temperature(void) {
	if (Bmp280.bmp_i2c->State == HAL_I2C_STATE_READY) {
		Temperature = BMP280_ReadTemperature(&Bmp280);
	}

	if (firstMeasurement){
		lowestTemperature = Temperature;
		firstMeasurement = false;
	}

	if (Temperature < lowestTemperature){
		lowestTemperature = Temperature;
	}

	if (Temperature > highestTemperature){
		highestTemperature = Temperature;
	}
	return Temperature;
}
static void display_temperatures(void) {
	SSD1306_Clear(BLACK);
	display_time();

	sprintf(Message, "Temp: %.2f", Temperature);
	GFX_DrawString(0, 10, Message, WHITE, 0);

	sprintf(Message, "Min: %.2f", lowestTemperature);
	GFX_DrawString(0, 20, Message, WHITE, 0);

	sprintf(Message, "Max: %.2f", highestTemperature);
	GFX_DrawString(0, 30, Message, WHITE, 0);

	SSD1306_Display();
	HAL_Delay(100);
}

static void display_time(void) {
	HAL_RTC_GetTime(&hrtc, &RtcTime, RTC_FORMAT_BIN);
	HAL_RTC_GetDate(&hrtc, &RtcDate, RTC_FORMAT_BIN);

	sprintf(Message, "%02d:%02d:%02d", RtcTime.Hours, RtcTime.Minutes,RtcTime.Seconds);
	GFX_DrawString(40, 0, Message, WHITE, 0);
}

static void display_menu(void) {
	SSD1306_Clear(BLACK);
	display_time();

	sprintf(Message, "1. Pomiar temperatury");
	GFX_DrawString(0, 10, Message, WHITE, 0);

	sprintf(Message, "2. Ustawienie zegara");
	GFX_DrawString(0, 20, Message, WHITE, 0);

	sprintf(Message, "3. Reset");
	GFX_DrawString(0, 30, Message, WHITE, 0);

	SSD1306_Display();
	HAL_Delay(100);
}

void read_keypad(void) {
	/* Make ROW 1 LOW and all other ROWs HIGH */
	HAL_GPIO_WritePin(R1_PORT, R1_PIN, GPIO_PIN_RESET);  //Pull the R1 low
	HAL_GPIO_WritePin(R2_PORT, R2_PIN, GPIO_PIN_SET);  // Pull the R2 High
	HAL_GPIO_WritePin(R3_PORT, R3_PIN, GPIO_PIN_SET);  // Pull the R3 High
	HAL_GPIO_WritePin(R4_PORT, R4_PIN, GPIO_PIN_SET);  // Pull the R4 High

	if (!(HAL_GPIO_ReadPin(C1_PORT, C1_PIN)))   // if the Col 1 is low
	{
		key = 4;
		while (!(HAL_GPIO_ReadPin(C1_PORT, C1_PIN)))
			;   // wait till the button is pressed
	}

	if (!(HAL_GPIO_ReadPin(C2_PORT, C2_PIN)))   // if the Col 2 is low
	{
		key = 1;
		while (!(HAL_GPIO_ReadPin(C2_PORT, C2_PIN)))
			;   // wait till the button is pressed
	}

	if (!(HAL_GPIO_ReadPin(C3_PORT, C3_PIN)))   // if the Col 3 is low
	{
		key = 2;
		while (!(HAL_GPIO_ReadPin(C3_PORT, C3_PIN)))
			;   // wait till the button is pressed
	}

	if (!(HAL_GPIO_ReadPin(C4_PORT, C4_PIN)))   // if the Col 4 is low
	{
		key = 3;
		while (!(HAL_GPIO_ReadPin(C4_PORT, C4_PIN)))
			;   // wait till the button is pressed
	}

	/* Make ROW 2 LOW and all other ROWs HIGH */
	HAL_GPIO_WritePin(R1_PORT, R1_PIN, GPIO_PIN_SET);  //Pull the R1 low
	HAL_GPIO_WritePin(R2_PORT, R2_PIN, GPIO_PIN_RESET);  // Pull the R2 High
	HAL_GPIO_WritePin(R3_PORT, R3_PIN, GPIO_PIN_SET);  // Pull the R3 High
	HAL_GPIO_WritePin(R4_PORT, R4_PIN, GPIO_PIN_SET);  // Pull the R4 High

	if (!(HAL_GPIO_ReadPin(C1_PORT, C1_PIN)))   // if the Col 1 is low
	{
		key = 8;
		while (!(HAL_GPIO_ReadPin(C1_PORT, C1_PIN)))
			;   // wait till the button is pressed
	}

	if (!(HAL_GPIO_ReadPin(C2_PORT, C2_PIN)))   // if the Col 2 is low
	{
		key = 5;

		while (!(HAL_GPIO_ReadPin(C2_PORT, C2_PIN)))
			;   // wait till the button is pressed
	}

	if (!(HAL_GPIO_ReadPin(C3_PORT, C3_PIN)))   // if the Col 3 is low
	{
		key = 6;
		while (!(HAL_GPIO_ReadPin(C3_PORT, C3_PIN)))
			;   // wait till the button is pressed
	}

	if (!(HAL_GPIO_ReadPin(C4_PORT, C4_PIN)))   // if the Col 4 is low
	{
		key = 7;
		while (!(HAL_GPIO_ReadPin(C4_PORT, C4_PIN)))
			;   // wait till the button is pressed
	}

	/* Make ROW 3 LOW and all other ROWs HIGH */
	HAL_GPIO_WritePin(R1_PORT, R1_PIN, GPIO_PIN_SET);  //Pull the R1 low
	HAL_GPIO_WritePin(R2_PORT, R2_PIN, GPIO_PIN_SET);  // Pull the R2 High
	HAL_GPIO_WritePin(R3_PORT, R3_PIN, GPIO_PIN_RESET);  // Pull the R3 High
	HAL_GPIO_WritePin(R4_PORT, R4_PIN, GPIO_PIN_SET);  // Pull the R4 High

	if (!(HAL_GPIO_ReadPin(C1_PORT, C1_PIN)))   // if the Col 1 is low
	{
		key = 12;
		while (!(HAL_GPIO_ReadPin(C1_PORT, C1_PIN)))
			;   // wait till the button is pressed
	}

	if (!(HAL_GPIO_ReadPin(C2_PORT, C2_PIN)))   // if the Col 2 is low
	{
		key = 9;
		while (!(HAL_GPIO_ReadPin(C2_PORT, C2_PIN)))
			;   // wait till the button is pressed
	}

	if (!(HAL_GPIO_ReadPin(C3_PORT, C3_PIN)))   // if the Col 3 is low
	{
		key = 10;
		while (!(HAL_GPIO_ReadPin(C3_PORT, C3_PIN)))
			;   // wait till the button is pressed
	}

	if (!(HAL_GPIO_ReadPin(C4_PORT, C4_PIN)))   // if the Col 4 is low
	{
		key = 11;
		while (!(HAL_GPIO_ReadPin(C4_PORT, C4_PIN)))
			;   // wait till the button is pressed
	}

	/* Make ROW 4 LOW and all other ROWs HIGH */
	HAL_GPIO_WritePin(R1_PORT, R1_PIN, GPIO_PIN_SET);  //Pull the R1 low
	HAL_GPIO_WritePin(R2_PORT, R2_PIN, GPIO_PIN_SET);  // Pull the R2 High
	HAL_GPIO_WritePin(R3_PORT, R3_PIN, GPIO_PIN_SET);  // Pull the R3 High
	HAL_GPIO_WritePin(R4_PORT, R4_PIN, GPIO_PIN_RESET);  // Pull the R4 High

	if (!(HAL_GPIO_ReadPin(C1_PORT, C1_PIN)))   // if the Col 1 is low
	{
		strcpy(mode, "resetValues");
		key = 16;
		while (!(HAL_GPIO_ReadPin(C1_PORT, C1_PIN)))
			;   // wait till the button is pressed
	}

	if (!(HAL_GPIO_ReadPin(C2_PORT, C2_PIN)))   // if the Col 2 is low
	{
		strcpy(mode, "menu");
		key = 13;
		while (!(HAL_GPIO_ReadPin(C2_PORT, C2_PIN)))
			;   // wait till the button is pressed
	}

	if (!(HAL_GPIO_ReadPin(C3_PORT, C3_PIN)))   // if the Col 3 is low
	{
		strcpy(mode, "measurement");
		key = 14;
		while (!(HAL_GPIO_ReadPin(C3_PORT, C3_PIN)))
			;   // wait till the button is pressed
	}

	if (!(HAL_GPIO_ReadPin(C4_PORT, C4_PIN)))   // if the Col 4 is low
	{
		strcpy(mode, "clockSet");
		key = 15;
		while (!(HAL_GPIO_ReadPin(C4_PORT, C4_PIN)))
			;   // wait till the button is pressed
	}
}

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
	/* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
